#!/bin/bash

script_dir=../js
log_dir=../log

nodejs ${script_dir}/server.js > ${log_dir}/node.log 2>&1 &
pid=`pgrep nodejs`
echo "node process id: ${pid}" 
