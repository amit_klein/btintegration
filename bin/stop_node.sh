#!/bin/bash


pid=`pgrep nodejs`
if [ -n "${pid}" ]
then
    kill -15 $pid
else
    echo "node is not running"
    exit 0
fi
sleep 2
pid=`pgrep nodejs`
if [ -n "${pid}" ]
then
    echo "failed to kill node process: $pid"
    exit 1
fi

echo "node server is down"

    
