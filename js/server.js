var express = require('express');
var url = require('url');
var app = express();
var fs = require('fs');
var globalPath='/html';


var server = app.listen(9091, function() {
	var host = server.address().address;
	var port = server.address().port;

	console.log('Server running at http://%s:%s/', host, port);
});


app.get('/test/:id', function(req,res) {
    console.log("got test request + param:%s", req.params.id);
    res.send("test successfull");
});

app.get('/ui/payment', function(req,res) {
	res.sendFile(__dirname + globalPath + "/checkout.html");
});

app.get('/ui/new_customer', function(req,res) {
	res.sendFile(__dirname + globalPath + "/new_customer.html");
});

app.get("/ui/dropin_checkout", function(req,res) {
	res.sendFile(__dirname + globalPath + "/dropin.html");
});

app.post("/payment", function(req,res) {
	var obj=null;
	req.on("data", function(data) {
		//console.log("payment data:%s", data);
		obj = JSON.parse(data);
	});
	req.on("end", function() {
		gateway.transaction.sale({
			amount: obj.amount,
			paymentMethodNonce: obj.token,
			options: {
				submitForSettlement: true
			}
		}, function(err, result) {
			if(result.success) {
				res.send("payment succeeded");
			}else{
				res.status(500).send("something wrong happened");
				console.log(err);
			}
		});
	});
});			

app.delete("/subscription/:id", function(req,res) {
     var sid = req.params.id;
    if (!sid || sid === undefined || sid.length <= 0 ) {
        res.status(400).send("missing subscription id parameter");
    }
    gateway.subscription.cancel(sid, function(err, customer) {
        if(customer) {
            res.status(200).send("OK");
        }else{
            log.console("Failed to cancel subscription:%s", err);
            res.status(404).send("Failed to cancel subscription");
        }
    });
});
app.post("/subscription", function(req,res) {
     var obj;
    req.on("data", function (data) {
        console.log("subscription data:%s", data);
        obj = JSON.parse(data);
        res.setHeader('Content-Type', 'text/html');
    });

    req.on("end", function() {
        gateway.subscription.create({
            paymentMethodToken: obj.token,
            planId: "mailnicks"
        }, function (err, result) {
            if(err) {
                console.log("failed to create subsciption");
                res.status(500).send("failed");
            }else if(result){
                console.log("subsciptoin created:%s", result);
                res.status(200).send("success");
            }
        })
    });
});

app.delete("/customer/:id", function(req,res) {
    var cid = req.params.id;
    if (!cid || cid === undefined || cid.length <= 0 ) {
        res.status(400).send("missing customer id parameter");
    }
    gateway.customer.delete(cid, function (err) {
        if(!err) {
            res.status(200).send("deleted");
        } else {
            console.log("failed to delete customer id %s:%s", cid, err);
            res.status(500).send("Failed to delete customer");
        }
    });
});
app.get("/customer/:id", function (req,res) {
    var cid = req.params.id;
    if (!cid || cid === undefined || cid.length <= 0 ) {
        res.status(400).send("missing customer id parameter");
    }
    gateway.customer.find(cid, function(err, customer) {
        if(customer) {
            res.status(200).send(JSON.stringify(customer));
        }else{
            log.console("Failed to find customer:%s", err);
            res.status(404).send("Costumer id not found");
        } 
    });
});

app.post("/customer", function (req,res) {
	var obj;
	req.on("data", function (data) {
		//console.log("data:%s", data);
		obj = JSON.parse(data);
		res.setHeader('Content-Type', 'text/html');
	});
	
	req.on("end", function() {
		gateway.customer.create({
			firstName: obj.firstName,
			lastName: obj.lastName,
            paymentMethodNonce: obj.token
			//paymentMethodNonce: "fake-valid-mastercard-nonce"
		}, function (err, result) {
			if(result && result.success === true) { 
            	res.status(200).send(result.customer.id);
			}else{
				res.status(401).send("something went wrong");
				console.log("error:%s", err);
			}
		});
	});	
});

app.get("/client_token", function (req, res) {
  	gateway.clientToken.generate({}, function (err, response) {
    	    res.send(response.clientToken);
  	});
});

// Console will print the message
var braintree = require("braintree");
var gateway = braintree.connect({
  environment: braintree.Environment.Sandbox,
  merchantId: "7ys2yqqrydx3n6h3",
  publicKey: "dtqxfyhqfdzp97db",
  privateKey: "f34d43ddb0a5de6828473f5dd34ab23a"
});
