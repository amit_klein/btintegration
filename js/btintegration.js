var getClientToken = function () {
	$.ajax({
    	type:"GET",
        url: nodeServer+"/client_token",
        async: false,
        sucess: updateToken
		error: function(data) {
			console.log("Failed to get client token: %s", data);
		}
    }).success(function (data) {
		console.log("Got client token")
		return data;
    });
}
